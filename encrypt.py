# SSIDとパスワードを"/"で区切り、暗号化。

# ------要変更---------
ssid = "aterm-271g-4w"
psw = "ajfh189rhu18&"
# 暗号化するための公開鍵を入力。 generate_rsa_key.pyで生成。
public_key = (7, 407)
# ------ここまで--------

plain_text = ssid + '/' + psw

# 暗号化処理
def encrypt_main(text, p_key):
	E, N = p_key
	plain_integers = [ord(char) for char in text]
	encrypted_integers = [pow(i, E, N) for i in plain_integers]
	hex_num = [hex(n)[2:].zfill(3) for n in encrypted_integers]
	encrypted_text = ''.join(hex_num)
	return encrypted_text


if __name__ == '__main__':
	encrypted = encrypt_main(plain_text, public_key)
	# 暗号文を表示。手動でQRコードを生成。
	print(encrypted)