import sys

args = sys.argv
encrypted_text = args[1]
# --------要変更----------
private_key = (103, 407)
# --------ここまで---------
wpa_path = './wpa_supplicant_auto.conf'


## 復号
def decrypt(p_key, s):
	print("復号前：", encrypted_text)
	D, N = p_key
	#s = encrypted_text 
	encrypted_integers = [s[i: i+3] for i in range(0, len(s), 3)]
	decrypted_intergers = [pow(int(i, 16), D, N) for i in encrypted_integers]
	decrypted_text = ''.join(chr(i) for i in decrypted_intergers)
	print("復号後：", decrypted_text)
	return decrypted_text

def split_ssid_psw(text):
	text_l = text.split("/")
	return text_l[0], text_l[1]

def make_wpa_conf(path, ssid, psw):
	msg = 'ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\nupdate_config=1\ncountry=JP\n\nnetwork={\n	ssid=' + ssid + '\n	psk=' + psw + '\n	priority=5\n}'
	f = open(path, 'w')
	f.write(msg)
	f.close()
	print("wpa_conf作成しました。")
	
if __name__ == '__main__':
	decrypted = decrypt(private_key, encrypted_text)
	wifi_ssid, password = split_ssid_psw(decrypted)
	make_wpa_conf(wpa_path, wifi_ssid, password)