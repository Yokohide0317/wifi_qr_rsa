import qrcode

# ------要変更---------
msg = "HelloWorld!!"
# 暗号化するための公開鍵を入力。 generate_rsa_key.pyで生成。
public_key = (7, 407)
# ------ここまで--------

plain_text = msg

# 暗号化処理
def encrypt_main(text, p_key):
	E, N = p_key
	plain_integers = [ord(char) for char in text]
	encrypted_integers = [pow(i, E, N) for i in plain_integers]
	hex_num = [hex(n)[2:].zfill(3) for n in encrypted_integers]
	encrypted_text = ''.join(hex_num)
	return encrypted_text

def make_qr(text):
	img = qrcode.make(text)
	img.save("./qrcode.png")

if __name__ == '__main__':
	encrypted = encrypt_main(plain_text, public_key)
	# 暗号文を表示。QRコードも同ディレクトリで生成
	print(encrypted)
	make_qr(encrypted)
	print("png image was created.")
