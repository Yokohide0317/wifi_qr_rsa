import cv2
import numpy as np

private_key = (103, 407)

# 秘密鍵を使って復号。
def decrypt(p_key, s):
	D, N = p_key
	encrypted_integers = [s[i: i+3] for i in range(0, len(s), 3)]
	decrypted_intergers = [pow(int(i, 16), D, N) for i in encrypted_integers]
	decrypted_text = ''.join(chr(i) for i in decrypted_intergers)
	return decrypted_text

# -----------------------------------------------------------
# カメラ部分お借りしました。
# https://qiita.com/PoodleMaster/items/0afbce4be7e442e75be6
# -----------------------------------------------------------
font = cv2.FONT_HERSHEY_SIMPLEX
cap = cv2.VideoCapture(0)
qrd = cv2.QRCodeDetector()

while cap.isOpened():
    ret, frame = cap.read()
    if ret:
        # QRコードデコード
        retval, decoded_info, points, straight_qrcode = qrd.detectAndDecodeMulti(frame)
        if retval:
            points = points.astype(np.int32)
            for dec_inf, point in zip(decoded_info, points):
                if dec_inf == '':
                    continue
                ## ----------復号-------------
                text = decrypt(private_key, dec_inf)

                # QRコード座標取得
                x = point[0][0]
                y = point[0][1]
                # QRコードデータ
                print('dec:', text)
                frame = cv2.putText(frame, text, (x, y - 6), font, .8, (0, 0, 255), 1, cv2.LINE_AA)
                # バウンディングボックス
                frame = cv2.polylines(frame, [point], True, (0, 255, 0), 1, cv2.LINE_AA)
        # 画像表示
        cv2.imshow('cv2', frame)
    # quit
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# キャプチャリソースリリース
cap.release()

