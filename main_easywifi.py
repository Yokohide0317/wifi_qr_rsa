import cv2
from pyzbar.pyzbar import decode, ZBarSymbol
from math import gcd
import os
import subprocess

cap = cv2.VideoCapture(0)

## 変更部分
private_key = (103, 407)
wpa_path = './wpa_supplicant_test.conf'

## 復号
def decrypt(p_key, s):
	print("復号前：", s)
	D, N = p_key
	encrypted_integers = [s[i: i+3] for i in range(0, len(s), 3)]
	decrypted_intergers = [pow(int(i, 16), D, N) for i in encrypted_integers]
	decrypted_text = ''.join(chr(i) for i in decrypted_intergers)
	print("復号後：", decrypted_text)
	return decrypted_text

## ssidとパスワードの精製
def split_ssid_psw(text):
	text_l = text.split("/")
	return text_l[0], text_l[1]

## wpa_supplicant.confの作成
def make_wpa_conf(path, ssid, psw):
	msg = 'ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\nupdate_config=1\ncountry=JP\n\nnetwork={\n	ssid=' + ssid + '\n	psk=' + psw + '\n	priority=5\n}'
	f = open(path, 'w')
	f.write(msg)
	f.close()
	print("wpa_conf作成しました。")

def move_conf(file_path):
	#shutil.copyfile(file_path, "/etc/wpa_supplicant/wpa_supplicant.conf")
	subprocess.call( ["/bin/sh", "./restart_wpa.sh"] )
	

if __name__ == '__main__':
    os.chdir("/home/pi/easy_wifi_rsa/")	
	## --カメラで認識--
    while cap.isOpened():
        ret, frame = cap.read()

        if ret:
            # デコード
            value = decode(frame, symbols=[ZBarSymbol.QRCODE])

            if value:
                for qrcode in value:
                    # QRコードデータ
                    dec_inf = qrcode.data.decode('utf-8')
                    print('QR検出')
                break

            # 画像表示
            # デバック用。コメントアウト必須
            #cv2.imshow('pyzbar', frame)
        # quit
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
        ## ----
    
    # 解読
    decrypted = decrypt(private_key, dec_inf)
    wifi_ssid, password = split_ssid_psw(decrypted)
    make_wpa_conf(wpa_path, wifi_ssid, password)
    move_conf(wpa_path)
