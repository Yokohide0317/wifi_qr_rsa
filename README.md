# wifi_qr_rsa
## 概要
ラズパイでwpa_supplicant.confをQRから作成。<br><br>
SSID/PASSWORD<br>
の文字列を事前に公開鍵で暗号化し、QRコードを生成。<br>
ラズパイ側では起動時にQRコード認識＆デコードのコードを実行しておくことで、<br>
シェルに入らずに(比較的)安全にWiFiを書き換えることができます。<br>

## 必要モジュール
opencv<br>
libzbar0 (Linuxの場合。macではzbar)<br>
pyzbar<br>

## 実行する前に
main_easywifi.pyの、

~~~
cv2.imshow('pyzbar', frame)
~~~

をコメントアウト<br>
↑↑はGUI上での動作テストに使える。<br>

## 秘密鍵・公開鍵の作成
generate_rsa_key.pyのpとqを適宜変更。<br>
出力された鍵を、encrypt.py及びmain_easywifi.py内でも変更<br>

## SSIDとパスワードから暗号文を作成
encrypt.pyのssidとpswを変更。<br>
出力された文字列をQRコード作成ツールを使って生成。<br>
[https://qr.quel.jp/]<br>

## 実行
main_easywifi.pyを実行。<br>
カメラにQRコードを向ける。<br>
