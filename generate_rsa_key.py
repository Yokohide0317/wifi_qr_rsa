# 暗号化および復号に必要な鍵の生成。
from math import gcd

# ------要変更---------
# pとqは素数3桁より小さい素数にしないと、
# 文字を16進数に変換するときに4桁になる。
p = 37
q = 11
# ------ここまで--------

def lcm(p, q):  
  return (p * q) // gcd(p, q)

def generate_keys(p, q):
  N = p * q
  L = lcm(p - 1, q - 1)

  for i in range(2, L):
    if gcd(i, L) == 1:
      E = i
      break

  for i in range(2, L):
    if (E * i) % L == 1:
      D = i
      break

  return (E, N), (D, N)

if __name__ == '__main__':

    public_key, private_key = generate_keys(p, q)
    # 表示された公開鍵と秘密鍵をそれぞれコードに組み込む。
    # 公開鍵：encrypt.py
    # 秘密鍵：decrypt.py
    print("public_key：", public_key)
    print("private_key：", private_key)